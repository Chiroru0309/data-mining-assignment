const express = require('express');
const router = new express.Router();
const thoitiets = require('../controllers/thoitiets.js');


router.route('/thoitiets/:id?')
  .get(thoitiets.get);

module.exports = router;