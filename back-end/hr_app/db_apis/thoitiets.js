const database = require('../services/database.js');
const oracledb = require('oracledb');
const baseQuery =
//  `select id "id",
//     first_name "first_name", 
//     last_name "last_name",
//     email "email",
//     phone_number "phone_number",
//     hire_date "hire_date",
//     job_id "job_id",
//     salary "salary",
//     commission_pct "commission_pct",
//     manager_id "manager_id",
//     department_id "department_id"
//   from thoitiets`;
`SELECT
*
  from thoitiet; `;

  async function find(context) {
    let query = baseQuery;
    const binds = {};
  
    if (context.id) {
      binds.thoitiet_id = context.id;
  
      query += `\nwhere thoitiet_id = :thoitiet_id`;
    }
  
    const result = await database.simpleExecute(query,binds);
  
    return result.rows;
  }
  
  module.exports.find = find;

