

CREATE TABLE thoitiet (
  id                    NUMBER(15)      DEFAULT emp_seq.nextval NOT NULL,
  ngay                  DATE          NOT NULL,
  nhietdotrungbinh      NUMBER(10)    NOT NULL,
  doam                  NUMBER(10)    NOT NULL,
  tocdogio              NUMBER(10)    NOT NULL,
  thoigianchieusang     NUMBER(10)    NOT NULL,
  trangthai             VARCHAR(50)   NOT NULL,
  CONSTRAINT pk PRIMARY KEY (id)
 );
 

 GRANT select, insert, update ON hr.thoitiet TO sec_admin;
GRANT select, insert, update, delete ON hr.thoitiet TO hr_sec WITH GRANT OPTION;
COMMIT;
 
 insert into thoitiet(id,ngay,nhietdotrungbinh,doam,tocdogio,thoigianchieusang,trangthai) values (1,DATE '2019-11-1',21.4,50.9,3.0,8.5,'N?ng');
 insert into thoitiet(id,ngay,nhietdotrungbinh,doam,tocdogio,thoigianchieusang,trangthai) values (2,DATE '2019-11-2',19.4,56.1,2.8,8.1,'M�t');
 insert into thoitiet(id,ngay,nhietdotrungbinh,doam,tocdogio,thoigianchieusang,trangthai) values (3,DATE '2019-11-3',20.5,49.0,3.3,9.5,'N?ng');
 insert into thoitiet(id,ngay,nhietdotrungbinh,doam,tocdogio,thoigianchieusang,trangthai) values (4,DATE '2019-11-4',20.5,39.9,3.0,9.9,'N?ng');
 insert into thoitiet(id,ngay,nhietdotrungbinh,doam,tocdogio,thoigianchieusang,trangthai) values (5,DATE '2019-11-5',20.1,60.7,2.9,7.0,'M�t');
 insert into thoitiet(id,ngay,nhietdotrungbinh,doam,tocdogio,thoigianchieusang,trangthai) values (6,DATE '2019-11-6',19.1,58.2,3.0,9.1,'M�t');
 insert into thoitiet(id,ngay,nhietdotrungbinh,doam,tocdogio,thoigianchieusang,trangthai) values (7,DATE '2019-11-7',19.8,53.3,2.9,9.8,'N?ng');
 insert into thoitiet(id,ngay,nhietdotrungbinh,doam,tocdogio,thoigianchieusang,trangthai) values (8,DATE '2019-11-8',20.7,70.3,3.5,7.6,'M?a');
 insert into thoitiet(id,ngay,nhietdotrungbinh,doam,tocdogio,thoigianchieusang,trangthai) values (9,DATE '2019-11-9',19.1,56.6,3.0,10.2,'M?a');
 insert into thoitiet(id,ngay,nhietdotrungbinh,doam,tocdogio,thoigianchieusang,trangthai) values (10,DATE '2019-11-10',22.5,56.9,3.4,9.6,'N?ng');
 
select * from nls_database_parameters;

 SELECT
id "id",
ngay "ngay",
nhietdotrungbinh "nhietdotrungbinh",
doam "doam",
tocdogio "tocdogio",
thoigianchieusang "thoigianchieusang",
trangthai "trangthai"
  from thoitiet; 
  select * from thoitiet;
  
  
 