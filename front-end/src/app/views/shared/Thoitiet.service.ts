import { Injectable } from '@angular/core';
import {Http, Response,  Headers, RequestOptions, RequestMethod} from '@angular/http';
import {Thoitiet} from '../models/thoitiet.class';
import { Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpHeaders,HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class UserService{
    constructor(public http: Http){}
    getUsers(){
       let headers = new Headers({'Content-Type':'application/jsonp'});
        // headers.append('Access-Control-Allow-Methods', 'GET');
        // headers.append('Access-Control-Allow-Origin', '*');
         let options = new RequestOptions({headers:headers});
         return this.http.get('http://localhost:4200/api/thoitiets',options);
       
    }

    postUsers(usr:Thoitiet):Observable<Thoitiet>{
        // let headers = new Headers({'Content-Type':'application/json'});
        // let options = new RequestOptions({headers:headers});
        // return this.http.post('http://5d4a5a4f5c331e00148eb0bb.mockapi.io/employee/',options)
        // .pipe(map(
        //     (
        //       res:Response)=> res.json()
        //     ));
        return null;
    }

    putUsers(usr:Thoitiet):Observable<Thoitiet>{
        // let headers = new Headers({'Content-Type':'application/json'});
        // let options = new RequestOptions({headers:headers});
        // return this.http.put('http://5d4a5a4f5c331e00148eb0bb.mockapi.io/employee/'+usr.id,usr,options)
        // .pipe(map(
        //     (
        //       res:Response)=> res.json()
        //     ));
        return null;
    }

    deleteUsers(usr:Thoitiet):Observable<Thoitiet>
    {
        // let headers = new Headers({'Content-Type':'application/json'});
        // let options = new RequestOptions({headers:headers});
        // return this.http.delete('http://5d4a5a4f5c331e00148eb0bb.mockapi.io/employee/'+usr.id)
        // .pipe(map(
        //     (
        //       res:Response)=> res.json()
        //     ));
        return null;
    }

}