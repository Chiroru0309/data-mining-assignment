export class Thoitiet
{
    id: number;
    ngay: Date;
    nhietdotrungbinh: number;
    tocdogio: number;
    doam: number;
    thoigianchieusang:number;
    trangthai: string;
}