import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuanlyKhachhangComponent } from './quanly-khachhang.component';

const routes: Routes = [
  {
    path: '',
    component: QuanlyKhachhangComponent,
    data: {
      title: 'Dự báo thời tiết'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuanlyKhachhangRoutingModule {}
