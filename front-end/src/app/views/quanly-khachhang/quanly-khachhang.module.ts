import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridModule } from '../../../../node_modules/@progress/kendo-angular-grid';
import { QuanlyKhachhangComponent } from './quanly-khachhang.component';
import { FormsModule } from '@angular/forms';
import { MessageModule } from 'primeng/message';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GalleriaModule } from 'primeng/galleria';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { QuanlyKhachhangRoutingModule } from './quanly-khachhang-routing.module';
import { ButtonModule } from '@progress/kendo-angular-buttons';
import { DialogsModule, WindowModule } from '@progress/kendo-angular-dialog';
import {ToastModule} from 'primeng/toast';
import { EventLogComponent } from './event-log/event-log.component';

@NgModule({
    imports:[
        FormsModule,
        MessageModule,
        GridModule,
        CommonModule,
        GalleriaModule,
        DateInputsModule,
        QuanlyKhachhangRoutingModule,
        ButtonModule,
        DialogsModule,
        ToastModule,
        WindowModule,
    ],
    declarations: [EventLogComponent,QuanlyKhachhangComponent, EventLogComponent]
})

export class QuanlyKhachhangModule{}