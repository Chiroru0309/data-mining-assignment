import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import {map} from 'rxjs/operators';
import 'rxjs/Rx';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html',
  styleUrls: ['register.component.scss']
})

export class RegisterComponent implements OnInit{
    signupform : FormGroup;
    ngay :FormControl;
    email : FormControl;
    fname : FormControl;
    lname : FormControl;
    password : FormControl;
    confirm : FormControl;
    //passwordFormGroup : FormGroup;
    base_link = 'http://www.localhost/Software-sharing-website/Back-End/index.php';
  	constructor(private http: Http){
      this.ngay =  new FormControl("",Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(255)
      ]));
      this.email = new FormControl("",Validators.compose([
        Validators.required,
        Validators.maxLength(255),
        Validators.email
      ]));
      this.fname = new FormControl("",Validators.compose([
        Validators.required,
        Validators.maxLength(255)
      ]));
      this.lname = new FormControl("",Validators.compose([
        Validators.required,
        Validators.maxLength(255)
      ]));
      this.password = new FormControl("",Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(255)
        
      ]));
      this.confirm = new FormControl("",Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(255),
      ]))
      this.signupform = new FormGroup({
        ngay : this.ngay,
        email : this.email,
        fname : this.fname,
        lname : this.lname,
        password : this.password,
        confirm : this.confirm
      });    

      console.log(this);
    }
  	ngOnInit() {}

    match_confirm() {
      let password = this.signupform.get('password').value;
      let confirmPassword = this.signupform.get('confirm').value;
      if (password == confirmPassword)
        return true;
      else{
        alert('Mật khẩu lần 2 nhập sai.');
        return false;
      }
    }


  	regist(){
        if (this.signupform.valid && this.match_confirm()) {
          console.log(this.signupform.value);
          //let headers = new Headers({'Content-Type' : 'application/json'});
          //let options = new RequestOptions({headers : headers});
          this.http.post(this.base_link + '?controller=Register&action=sign_up'
            ,this.signupform.value).pipe(map(response => response.json())).toPromise().then(
            data => {	
              alert("Đăng ký thành công!");
              console.log(data);
            },
            err => {
              console.log(err);
              alert(err.mess);
            }
          );
      }
      else alert("Hãy nhập đúng thông tin.");	
  	}
}
