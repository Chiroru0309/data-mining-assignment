import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Route, Router } from '@angular/router';
import { AuthService, LoginInfo } from '../shared/auth.service';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  erroMessage;
  public form: FormGroup;
  data: LoginInfo = {} as LoginInfo;
  constructor(private fb: FormBuilder,private router: Router, private authService: AuthService, private cookieService: CookieService) {    
  }
  public ngay: String='';
  public password: String='';
  ngOnInit() {
    
  }

  onSubmit()
  {  
    this.authService.login(this.data).subscribe(result =>{
      this.erroMessage = "";
      //save thoitiet infor into cookie

      this.authService.setLoggedIn(true);
      //redirect to home pagge
      this.router.navigate(['/trangchu']);
    },error=>{
    this.erroMessage = "wrong thoitiet name or password!"
  });  
  }
 }
