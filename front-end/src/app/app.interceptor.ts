import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { LoginInfo, AuthService } from "../app/views/shared/auth.service";
import {tap} from 'rxjs/operators';


@Injectable()
export class AppInterceptor implements HttpInterceptor{
    constructor(private route: Router, private cookieService: CookieService, private authService: AuthService)
    {}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        const strCookie  = this.cookieService.get('userInfo');
        if(strCookie)
        {
            const loginResult = JSON.parse(strCookie);
            if(loginResult)
            {
                const token = loginResult.token;
                const req = request.clone({
                    headers: new HttpHeaders().append('Authorization',token)
                })
                return next.handle(req).pipe(
                    tap(
                        event=>{},
                        error=>{
                            if(error instanceof HttpErrorResponse)
                            {
                                if(this.route.url != '/login' && (error.status===401))
                                {
                                    //vang ra login
                                }
                            }
                        }
                    )
                );
            }
        }       
        if ((this.authService.ten == 'hr' && this.authService.matkhau== '123456') || (this.authService.ten=='user1' && this.authService.matkhau == 'user1'))
        return next.handle(request);
        else return null;
    }
}